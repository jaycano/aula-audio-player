const jwt = require("jsonwebtoken");
const UserService = require("../services/user");

/**
 * User controller
 * @module controllers/user
 */

module.exports = {
  /**
   * Verifies the user credentials
   * @param {string} username - the account's username
   * @param {string} password - the account's password
   * @returns {Object} Returns error or object containing account information and the JWT
   */
  login: async (req, res, next) => {
    // Extract parameters from request body
    const { username, password } = req.body;

    // If the user is already authenticated, return 409 (conflict)
    if (req.isAuthenticated()) {
      return res
        .status(409)
        .send({ message: "Already logged in as" + req.user.username });
    }

    try {
      // Get the user to check the password
      const user = await UserService.getUserByUsername(username);

      // If the user doesn't exist, return 401 (unauthorized)
      if (!user) {
        return res.status(401).send({
          errors: "Incorrect username or password.",
          form_data: req.body,
        });
      }

      // Check the password
      const isValid = await user.checkPassword(password);

      // If password invalide, return 401 (unauthorized)
      if (!isValid) {
        return res.status(401).send({
          errors: "Incorrect username or password.",
          form_data: req.body,
        });
      }

      // Keep only the user id and username
      const returnUser = { id: user._id, username };

      // Return account info and JWT
      return res.send({
        ...returnUser,
        token: jwt.sign(returnUser, process.env.SESSION_SECRET),
      });
    } catch (e) {
      return next(e);
    }
  },

  /**
   * Lists all users
   * @param {string} username - a username to search for
   * @returns {Array<User>} Returns a list of users matching the search criteria
   */
  list: async (req, res, next) => {
    // Extract parameters from request body
    const { username } = req.query;

    // If the user is not authenticated or doesn't have privileges, return 401 (unauthorized)
    if (!req.isAuthenticated()) {
      return res
        .status(401)
        .send({ message: "Insufficient permissions to see users" });
    }

    try {
      // Get a list of the users
      const users = await UserService.list(username);

      // Return the list without the passwords
      return res.send(
        users.map(user => ({
          id: user._id,
          username: user.username,
        })),
      );
    } catch (e) {
      return next(e);
    }
  },
};
