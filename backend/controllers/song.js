const SongService = require("../services/song");

/**
 * Song controller
 * @module controllers/song
 */

module.exports = {
  /**
   * Lists all songs that a user can see
   * @param {string} title - the song's title
   * @param {string} artist - the song's artist
   * @param {string} album - the song's album
   * @param {number} track - the song's track number
   * @param {number} year - the song's year
   * @param {number} genre - the song's genre
   * @param {number} page - current page of the list
   * @param {number} limit - number of items per page
   * @param {string} sort - field to sort by
   * @returns {Array<Song>} Returns a list of songs with pagination information
   */
  list: async (req, res, next) => {
    // Extract parameters from request body
    const { title, artist, album, track, year, genre } = req.query;
    const page = parseInt(req.query.page) || 1;
    const limit = parseInt(req.query.limit) || 10;
    const sort = req.query.sort || "year";

    // Create a query object with the query parameters
    const query = Object.assign(
      {},
      title ? { title } : {},
      artist ? { artist } : {},
      album ? { album } : {},
      track ? { track } : {},
      year ? { year } : {},
      genre ? { genre } : {},
    );

    // If the user is not authenticated, return 401 (unauthorized)
    if (!req.isAuthenticated()) {
      return res
        .status(401)
        .send({ message: "Insufficient permissions to see these songs" });
    }

    try {
      // Fetch the songs and return them
      const songs = await SongService.list(query, page, limit, sort);
      return res.send({
        ...songs,
        docs: songs.docs.map(song => ({
          id: song.id,
          title: song.title,
          artist: song.artist,
          album: song.album,
          url: song.url,
          track: song.track,
          year: song.year,
          genre: song.genre,
          comment: song.comment,
        })),
      });
    } catch (e) {
      return next(e);
    }
  },

  /**
   * Shows a specific song
   * @param {string} songId -  the id of the song
   * @returns {Song} Returns the song, if it exists
   */
  show: async (req, res, next) => {
    // Extract parameters from URL params
    const { songId } = req.params;

    // If the user is not authenticated, return 401 (unauthorized)
    if (!req.isAuthenticated()) {
      return res
        .status(401)
        .send({ message: "Insufficient permissions to see the song" });
    }

    try {
      const song = await SongService.getSongById(songId);

      // If the song was not found
      if (!song) {
        return res.status(404).send({ message: "Song not found" });
      }

      // Return the song
      return res.json({
        id: song.id,
        title: song.title,
        artist: song.artist,
        album: song.album,
        url: song.url,
        track: song.track,
        year: song.year,
        genre: song.genre,
        comment: song.comment,
      });
    } catch (e) {
      return next(e);
    }
  },
};
