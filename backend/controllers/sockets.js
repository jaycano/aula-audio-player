exports = module.exports = function(io) {
  io.on("connection", function(socket) {
    socket.on("play", function(data) {
      console.log("PLAYING", data.username, data.song);
      socket.broadcast.emit("play", { ...data });
    });
  });
};
