const mongoose = require("mongoose");
const bcrypt = require("bcrypt");

const SALT_WORK_FACTOR = 12;

/**
 * User Model
 * @module models/user
 */

/**
 * User Schema
 */
const UserSchema = new mongoose.Schema({
  username: { type: String, required: true, unique: true, trim: true },
  password: { type: String, required: true },
});

/**
 * Hooks
 */

/**
 * Hash the password before storing the user object
 */
UserSchema.pre("save", function(next) {
  const user = this;

  // only hash the password if it has been modified (or is new)
  if (!user.isModified("password")) {
    return next();
  }

  // generate a salt
  return bcrypt.genSalt(SALT_WORK_FACTOR, (err, salt) => {
    if (err) {
      return next(err);
    }

    // hash the password using our new salt
    return bcrypt.hash(user.password, salt, (hashErr, hash) => {
      if (hashErr) {
        return next(hashErr);
      }

      // override the cleartext password with the hashed one
      user.password = hash;
      return next();
    });
  });
});

/**
 * Custom validators
 */

/** For username - Check the username is not blank */
UserSchema.path("username").validate(
  username => username && username.length > 0,
  "Username cannot be blank",
);

/** For username - Check the username has a valid format */
UserSchema.path("username").validate(
  username => /^[a-z][a-z0-9_-]{2,15}$/.test(username),
  "User names must start with a letter, be 2 to 15 characters long " +
    "and may only contain lowercase letters, numbers, underscore and dash",
);

/** For username - Check the username doesn't exist already */
UserSchema.path("username").validate({
  isAsync: true,
  validator: function(username) {
    return new Promise((resolve, reject) => {
      if (this.isModified("username")) {
        // Check if the username is already in the database
        mongoose
          .model("User")
          .findOne({ username: username.toLowerCase() }, (err, result) => {
            if (err || result) {
              resolve(false);
            } else {
              resolve(true);
            }
          });
      } else {
        resolve(true);
      }
    });
  },
  message: "Sorry, that username is not available",
});

/** For password - Enforce some password complexity */
UserSchema.path("password").validate(password => {
  const passwordPattern = /^(?=[\x20-\x7F]*\d)(?=[\x20-\x7F]*[a-z])(?=[\x20-\x7F]*[A-Z])[\x20-\x7F]{8,}$/;

  return passwordPattern.test(password);
}, "The password should be at least 8 characters long " + "and include a number, a lowercase letter and an uppercase letter");

/**
 * Instance methods
 */

/**
 * Compare a plain text password with a hash
 * @param {String} password - the password to verify
 * @returns {Promise<boolean>} - Returns if the password is correct or not, as a promise
 */
UserSchema.methods.checkPassword = function(password) {
  return bcrypt.compare(password, this.password);
};

/** Create the model if it doesn't exist */
if (!mongoose.models.User) {
  mongoose.model("User", UserSchema);
}
