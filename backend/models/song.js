const mongoose = require("mongoose");
const paginate = require("mongoose-paginate");

const ObjectId = mongoose.Schema.Types.ObjectId;

////////
// Schema to create song models on the database

const SongSchema = new mongoose.Schema({
  title: { type: String, required: true, trim: true },
  artist: { type: String, required: true, trim: true },
  album: { type: String, required: true, trim: true },
  url: { type: String, required: true, trim: true },
  track: { type: Number, default: 0 },
  year: { type: Number, default: 2018 },
  genre: { type: Number, default: 255 },
  comment: { type: String, trim: true },
});

// Add pagination plugin
SongSchema.plugin(paginate);

// Create the model if it doesn't exist
if (!mongoose.models.Song) {
  mongoose.model("Song", SongSchema);
}
