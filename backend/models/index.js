const mongoose = require("mongoose");

// Load the DB URL from an environment variable
const uristring = process.env.DATABASE_URL;

// Connect to the database
mongoose
  .connect(uristring)
  .then(() => console.log("Database -> success"))
  .catch(error => console.error("Database error:", error));

// Load the models
require("./user");
require("./song");

// Load fixtures when in development
if (process.env.NODE_ENV === "development") {
  // Load the libraries here to avoid cluttering unnecessarily
  const path = require("path");
  const fs = require("fs");

  // Once the database is connected, load the fixtures
  mongoose.connection.on("connected", () => {
    // We need to wait for each operation to finish before moving to the next,
    // but await/async only works with functions. So we create this IIFE to)
    // create the for loop, respecting the delete/load operation order.)
    // Alternatively, we could chain 'then' or use a library like async)
    (async model => {
      const fixturesPath = path.join(__dirname, "fixtures");
      const fixtures = {
        User: path.join(fixturesPath, "users.json"),
        Song: path.join(fixturesPath, "songs.json"),
      };

      try {
        for (let model in fixtures) {
          if (fixtures.hasOwnProperty(model)) {
            // First remove the existing data
            await mongoose.model(model).remove({});

            // Then add the fixtures from the fixtures file.
            await mongoose
              .model(model)
              .insertMany(JSON.parse(fs.readFileSync(fixtures[model]), "utf8"));
          }
        }
      } catch (e) {
        console.log("fixtures error:", e);
      }
    })();
  });
}
