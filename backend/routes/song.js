var express = require("express");
var router = express.Router();

var SongController = require("../controllers/song");

/**
 * Song Routes
 * @module routes/song
 */

router.get("/", SongController.list);
router.get("/:songId", SongController.show);

/**
 * Song router that handles the routes:
 * - GET / - list all songs
 * - GET /:songId - shows an song
 */
module.exports = router;
