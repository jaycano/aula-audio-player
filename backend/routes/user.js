const express = require("express");
const router = express.Router();

const UserController = require("../controllers/user");

/**
 * User Routes
 * @module routes/user
 */

router.post("/login", UserController.login);
router.get("/", UserController.list);

/**
 * User router that handles the routes:
 * - POST /login - login
 * - GET / - list all users
 */
module.exports = router;
