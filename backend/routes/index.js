const express = require("express");
const router = express.Router();

const userRouter = require("./user");
const songRouter = require("./song");

/* GET home page. */
router.get("/", function(req, res, next) {
  res.status(200).json({ message: "Backend works" });
});

router.use("/api/user", userRouter);
router.use("/api/song", songRouter);

module.exports = router;
