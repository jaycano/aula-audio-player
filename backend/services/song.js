const mongoose = require("mongoose");

const Song = mongoose.model("Song");

/**
 * User Services
 * @module services/user
 */

module.exports = {
  /**
   * Lists songs
   * @param {object} query - field values to filter the list
   * @param {number} page - the page to request
   * @param {number} limit - the number of songs per page
   * @param {string} sort - a field to sort by
   * @returns {Promise<Array<Song>>} Returns a list of songs, as a promise
   */
  list: (query = {}, page = 1, limit = 10, sort = "year") => {
    return Song.paginate(query, { page, limit, sort });
  },

  /**
   * Gets a song by the id, no permissions checked
   * @param {String} songId - The song's id
   * @returns {Promise<User>} Returns the song if found or error, as a promise
   */
  getSongById: songId => {
    return Song.findById(songId);
  },
};
