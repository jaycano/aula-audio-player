const mongoose = require("mongoose");

const User = mongoose.model("User");

/**
 * User Services
 * @module services/user
 */

module.exports = {
  /**
   * Gets a user by the id
   * @param {String} userId - The account's id
   * @returns {Promise<User>} Returns the user if found or error, as a promise
   */
  getUserById: userId => {
    return User.findById(userId);
  },

  /**
   * Gets a user by the username
   * @param {String} username - The account's username
   * @returns {Promise<User>} Returns the user if found or error, as a promise
   */
  getUserByUsername: username => {
    return User.findOne({ username });
  },

  /**
   * Lists all users
   * @param {String} username - a username to search for
   * @returns {Promise<Array<User>>} Returns a list of users, as a promise
   */
  list: username => {
    let query = {};

    // Add username to query if it exists
    if (username) {
      query.username = username;
    }

    return User.find(query);
  },
};
