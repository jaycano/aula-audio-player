const createError = require("http-errors");
const express = require("express");
const path = require("path");
const logger = require("morgan");
const jwt = require("express-jwt");
const cors = require("cors");
const helmet = require("helmet");

require("./models");
const routes = require("./routes");

const app = express();

/**
 * Main App
 */

// Prepare logging middleware
app.use(logger("dev"));

// Enable Cross-origin requests from authorized sources
app.use(cors({ origin: process.env.CORS_ORIGINS }));

// Use helmet to improve security. In addition to this, we should also add, at least, a CSP
app.use(helmet());

// Prepare body parsing middleware (loads querystrings and post bodies in the request object)
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// Add a static directory to serve files from
app.use(express.static(path.join(__dirname, "public")));

// Add JWT middleware
// SESSION_SECRET is the secret used to sign the token, set as an env variable
app.use(
  jwt({
    secret: process.env.SESSION_SECRET,
    credentialsRequired: false,
  }),
);

// Add the isAuthenticated method to check credentials
app.use(function(req, res, next) {
  req.isAuthenticated = function() {
    if (req.user) {
      return true;
    }

    return false;
  };

  return next();
});

// Load the routes
app.use("/", routes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // return the error
  res.status(err.status || 500).json(err);
});

module.exports = app;
