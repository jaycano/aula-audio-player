# AULA AUDIO PLAYER

## Backend

The backend runs on Node.js, using Express and MongoDB. It currently allows for authentication (using JWT tokens), and listing and retrieving songs.

For ease of use, the whole application runs on Docker and Docker Compose. In order to run the application, just change to the root of the project and run

```
$ docker-compose up
```

The Docker Compose will spin up both, the app and MongoDB, and mount the backend directory as a volume so it can be used for development. With the environment running, you can go on your browser or API testing tool (e.g. Postman) to start interacting with the app.

Current routes available:

- Status check (GET): http://localhost:3000
- Login (POST): http://localhost:3000/api/user/login
- List Users (GET): http://localhost:3000/api/user
- List Songs (GET): http://localhost:3000/api/song
- Show a Song (GET): http://localhost:3000/api/song/:songId

**Login** accepts two users at the moment (oliver, jay) with the password `Aula1234`.

**List Users** accepts username as a query parameter to search for that username.

**List Songs** accepts the following query parameters:

- title
- artist
- album
- track
- year
- genre

In addition to the pagination control arguments:

- page: current page of the list
- limit: number of items per page
- sort: field to sort by

None of the lookup fields for users or songs (e.g. username, title, album) are full text searches, they look for an exact match.

All operations except Login and Status require a JWT to be set on the `Authorization` header. For example:

```

Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVhNjg1MjY5ZTc0YmIyMjQ5YTExMWRmMyIsInVzZXJuYW1lIjoib2xpdmVyIiwiaWF0IjoxNTMzNzE3NjE5fQ.eXs8W3Q_0Xv1R9N0lNUNABxAJTfefSznqWKBOhMCXdU


```

## Frontend
To run the frontend, go into the frontend folder and run:

```
$ yarn install && yarn start
```

The dev server will start at http://localhost:5000

The app will open on a login screen. There are 2 users, jay and oliver, and the password for both is `Aula1234`. Once logged in, there are 2 songs available, click on the play button next to them to start playing. The button will turn into a pause button so you can press it to pause.

Sockets have been implemented to keep track of who's playing songs at the moment. When playing a song, it will show up on the "Currently Playing" box. Try opening the app on another browser (or incognito window) to see it in action.

## Highlights
- Authentication done with JWT
- HOCs to control the authenticated state
- Authenticated state persisted in localStorage
- Sagas for asynchronous state changes
- Socket.io implemention of a "currently playing" view (naive, no persistence)
- Separate routes for the views (login, songs)
- Loading and empty state for playlist
- Toasts to show errors on login
- Material UI with themes and some custom styles

## Future improvements
- Testing, testing testing
- Use webpack route aliases to clean the imports up
- The `Playlist` component is a bit crowded, probably needs splitting
- The backend supports paging the playlist, but it's not supported on the frontend
- Improve error handling (currently only handled on login)

