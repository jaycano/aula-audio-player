import React, { Component } from "react";
import { connect } from "react-redux";
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  Switch,
} from "react-router-dom";

import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import { purple, green } from "@material-ui/core/colors";
import CssBaseline from "@material-ui/core/CssBaseline";

import Navbar from "../components/Navbar";
import Login from "../../auth/containers/Login";
import { withAuthentication, withNoAuthentication } from "../../auth/hocs";
import { actions as authActions } from "../../auth/redux";
import Songs from "../../songs/containers/Songs";

const theme = createMuiTheme({
  palette: {
    primary: purple,
    secondary: green,
  },
});

class App extends Component {
  componentDidMount() {
    this.props.getAuthState();
  }

  render() {
    return (
      <Router>
        <React.Fragment>
          <CssBaseline />
          <MuiThemeProvider theme={theme}>
            <Navbar />

            <Switch>
              <Route path="/login" component={withNoAuthentication(Login)} />
              <Route path="/songs" component={withAuthentication(Songs)} />
              <Redirect to="/songs" />
            </Switch>
          </MuiThemeProvider>
        </React.Fragment>
      </Router>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  getAuthState: () => dispatch(authActions.restoreAuthState()),
});

export default connect(
  null,
  mapDispatchToProps,
)(App);
