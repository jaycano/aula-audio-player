import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

import { withStyles } from "@material-ui/core/styles";
import { AppBar, Button, Toolbar, Typography } from "@material-ui/core";

const styles = {
  flex: {
    flexGrow: 1,
  },
};

const Navbar = props => {
  const { classes } = props;

  return (
    <AppBar position="static">
      <Toolbar variant="dense">
        <Typography variant="title" color="inherit" className={classes.flex}>
          Aula Audio Player
        </Typography>
        <Button color="inherit" component={Link} to="/login">
          Login
        </Button>
      </Toolbar>
    </AppBar>
  );
};

Navbar.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Navbar);
