import React from "react";
import { connect } from "react-redux";
import withStyles from "@material-ui/core/styles/withStyles";

import { actions, selectors } from "../redux";

import LoginForm from "../components/LoginForm";

const styles = theme => ({
  layout: {
    width: "auto",
    display: "block", // Fix IE11 issue.
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 400,
      marginLeft: "auto",
      marginRight: "auto",
    },
  },
});

const Login = props => {
  const { classes, handleSubmit, isLoading, error } = props;

  return (
    <main className={classes.layout}>
      <LoginForm
        handleSubmit={handleSubmit}
        isLoading={isLoading}
        error={error}
      />
    </main>
  );
};

const mapStateToProps = state => {
  return {
    isLoading: selectors.getIsLoading(state),
    error: selectors.getError(state),
  };
};

const mapDispatchToProps = dispatch => {
  return {
    handleSubmit: (username, password) => {
      return dispatch(actions.login(username, password));
    },
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(styles)(Login));
