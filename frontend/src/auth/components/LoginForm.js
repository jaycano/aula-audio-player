import React from "react";
import PropTypes from "prop-types";

import {
  Button,
  FormControl,
  Input,
  InputLabel,
  Paper,
  Snackbar,
  SnackbarContent,
} from "@material-ui/core";
import ErrorIcon from "@material-ui/icons/Error";
import { withStyles } from "@material-ui/core/styles";

const styles = theme => ({
  paper: {
    marginTop: theme.spacing.unit * 8,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme
      .spacing.unit * 3}px`,
  },
  avatar: {
    width: 256,
    height: 128,
  },
  form: {
    width: "100%", // Fix IE11 issue.
    marginTop: theme.spacing.unit,
  },
  submit: {
    marginTop: theme.spacing.unit * 3,
  },
  error: {
    backgroundColor: theme.palette.error.dark,
  },
  errorContent: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-around",
  },
  errorIcon: {
    marginRight: theme.spacing.unit,
  },
});

class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      hideError: false,
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.closeToast = this.closeToast.bind(this);
  }

  closeToast() {
    this.setState({ hideError: true });
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleSubmit(event) {
    const { username, password } = this.state;
    event.preventDefault();
    this.setState({ hideError: false });
    this.props.handleSubmit(username, password);
  }

  render() {
    const { classes } = this.props;

    return (
      <Paper className={classes.paper}>
        <Snackbar
          anchorOrigin={{
            vertical: "top",
            horizontal: "right",
          }}
          open={!!this.props.error && !this.state.hideError}
          onClose={this.closeToast}
          autoHideDuration={3000}
        >
          <SnackbarContent
            message={
              <span className={classes.errorContent}>
                <ErrorIcon className={classes.errorIcon} />
                {this.props.error}
              </span>
            }
            className={classes.error}
          />
        </Snackbar>
        <img src="/aula-logo.svg" className={classes.avatar} alt="Aula Logo" />
        <form className={classes.form} onSubmit={this.handleSubmit}>
          <FormControl margin="normal" required fullWidth>
            <InputLabel htmlFor="username">Username</InputLabel>
            <Input
              id="username"
              name="username"
              autoComplete="username"
              autoFocus
              onChange={this.handleChange}
            />
          </FormControl>
          <FormControl margin="normal" required fullWidth>
            <InputLabel htmlFor="password">Password</InputLabel>
            <Input
              id="password"
              name="password"
              type="password"
              autoComplete="current-password"
              onChange={this.handleChange}
            />
          </FormControl>
          <Button
            type="submit"
            variant="raised"
            color="primary"
            className={classes.submit}
            fullWidth
          >
            Login
          </Button>
        </form>
      </Paper>
    );
  }
}
LoginForm.propTypes = {
  classes: PropTypes.object.isRequired,
  error: PropTypes.string,
  isLoading: PropTypes.bool.isRequired,
};

export default withStyles(styles)(LoginForm);
