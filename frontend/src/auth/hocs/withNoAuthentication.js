import React from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import { selectors } from "../redux";

export default function(ComponentToWrap) {
  const WrappedComponent = props => {
    return (
      <React.Fragment>
        {!props.authToken ? (
          <ComponentToWrap {...props} />
        ) : (
          <Redirect to={{ pathname: "/", state: { from: props.location } }} />
        )}
      </React.Fragment>
    );
  };

  WrappedComponent.propTypes = {
    authToken: PropTypes.string,
  };

  const mapStateToProps = state => ({
    authToken: selectors.getToken(state),
  });

  return connect(
    mapStateToProps,
    null,
  )(WrappedComponent);
}
