import axios from "axios";

export function setAuthStateOnStorage(id, username, token) {
  localStorage.setItem("id", id);
  localStorage.setItem("username", username);
  localStorage.setItem("authToken", token);
}

export function getAuthStateFromStorage() {
  return {
    id: localStorage.getItem("id"),
    username: localStorage.getItem("username"),
    token: localStorage.getItem("authToken"),
  };
}

export function getAuthTokenFromStorage() {
  return localStorage.getItem("authToken");
}

export async function authenticatedGet(url) {
  const token = await getAuthTokenFromStorage();

  return axios.get(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
}

export default {
  setAuthStateOnStorage,
  getAuthStateFromStorage,
  getAuthTokenFromStorage,
  authenticatedGet,
};
