import axios from "axios";

export default {
  login: (username, password) => {
    return axios.post("http://localhost:3000/api/user/login", {
      username,
      password,
    });
  },
};
