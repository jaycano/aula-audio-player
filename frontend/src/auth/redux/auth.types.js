export const RESTORE_AUTH_STATE = "[auth] RESTORE_AUTH_STATE";
export const RESTORE_AUTH_STATE_SUCCESS = "[auth] RESTORE_AUTH_STATE_SUCCESS";
export const RESTORE_AUTH_STATE_FAILURE = "[auth] RESTORE_AUTH_STATE_FAILURE";
export const LOGIN = "[auth] LOGIN";
export const LOGIN_SUCCESS = "[auth] LOGIN_SUCCESS";
export const LOGIN_FAILURE = "[auth] LOGIN_FAILURE";
