export const selectAuthBranch = state => state.auth;

export const getIsLoading = state => selectAuthBranch(state).isLoading;
export const getError = state => selectAuthBranch(state).error;
export const getId = state => selectAuthBranch(state).id;
export const getUsername = state => selectAuthBranch(state).username;
export const getToken = state => selectAuthBranch(state).token;

export default {
  getIsLoading,
  getError,
  getId,
  getUsername,
  getToken,
};
