import {
  RESTORE_AUTH_STATE,
  RESTORE_AUTH_STATE_SUCCESS,
  RESTORE_AUTH_STATE_FAILURE,
  LOGIN,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
} from "./auth.types";

export const restoreAuthState = () => ({
  type: RESTORE_AUTH_STATE,
});

export const restoreAuthStateSuccess = authState => ({
  type: RESTORE_AUTH_STATE_SUCCESS,
  payload: { ...authState },
});

export const restoreAuthStateFailure = error => ({
  type: RESTORE_AUTH_STATE_FAILURE,
  payload: { error },
});

export const login = (username, password) => ({
  type: LOGIN,
  payload: {
    username,
    password,
  },
});

export const loginSuccess = user => ({
  type: LOGIN_SUCCESS,
  payload: {
    ...user,
  },
});

export const loginFailure = (error, username, password) => ({
  type: LOGIN_FAILURE,
  payload: {
    error,
    username,
    password,
  },
});

export default {
  restoreAuthState,
  restoreAuthStateSuccess,
  restoreAuthStateFailure,
  login,
  loginSuccess,
  loginFailure,
};
