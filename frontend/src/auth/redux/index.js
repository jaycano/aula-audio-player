import reducer from "./auth.reducers";

export { default as sagas } from "./auth.sagas";
export { default as actions } from "./auth.actions";
export { default as selectors } from "./auth.selectors";

export default reducer;
