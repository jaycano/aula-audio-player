import { call, put, takeLatest } from "redux-saga/effects";
import Api from "./auth.api";
import Utils from "../utils";
import actions from "./auth.actions";
import { LOGIN, RESTORE_AUTH_STATE } from "./auth.types";

function* restoreAuthState(action) {
  try {
    const authState = yield call(Utils.getAuthStateFromStorage);
    yield put(actions.restoreAuthStateSuccess(authState));
  } catch (e) {
    yield put(actions.restoreAuthStateFailure(e));
  }
}

function* login(action) {
  try {
    const { username, password } = action.payload;
    const response = yield call(Api.login, username, password);
    const { id, token } = response.data;
    yield put(
      actions.loginSuccess({
        username: username,
        id: id,
        token: token,
      }),
    );
    yield call(Utils.setAuthStateOnStorage, id, username, token);
  } catch (e) {
    const { errors, username, password } = e.response.data;
    yield put(actions.loginFailure(errors, username, password));
  }
}

function* authSaga() {
  yield takeLatest(LOGIN, login);
  yield takeLatest(RESTORE_AUTH_STATE, restoreAuthState);
}

export default authSaga;
