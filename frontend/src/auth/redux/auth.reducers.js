import {
  LOGIN,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  RESTORE_AUTH_STATE_SUCCESS,
} from "./auth.types";

const initialState = {
  isLoading: false,
  error: "",
  id: "",
  username: "",
  token: "",
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case RESTORE_AUTH_STATE_SUCCESS:
      return {
        ...initialState,
        id: action.payload.id,
        username: action.payload.username,
        token: action.payload.token,
      };
    case LOGIN:
      return {
        ...initialState,
        isLoading: true,
      };
    case LOGIN_SUCCESS:
      return {
        ...initialState,
        id: action.payload.id,
        username: action.payload.username,
        token: action.payload.token,
      };
    case LOGIN_FAILURE:
      return {
        ...initialState,
        error: action.payload.error,
      };
    default:
      return state;
  }
};

export default authReducer;
