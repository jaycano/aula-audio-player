import { createStore, combineReducers, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import createSagaMiddleware from "redux-saga";

import authReducer, { sagas as authSagas } from "./auth/redux";
import songsReducer, { sagas as songsSagas } from "./songs/redux";

const sagaMiddleware = createSagaMiddleware();

const reducers = combineReducers({
  auth: authReducer,
  songs: songsReducer,
});

const store = createStore(
  reducers,
  composeWithDevTools(applyMiddleware(sagaMiddleware)),
);

sagaMiddleware.run(authSagas);
sagaMiddleware.run(songsSagas);

export default store;
