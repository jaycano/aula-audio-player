export const selectSongsBranch = state => state.songs;

export const getIsLoading = state => selectSongsBranch(state).isLoading;
export const getError = state => selectSongsBranch(state).error;
export const getSongs = state => selectSongsBranch(state).songs;
export const getSelectedSong = state => selectSongsBranch(state).selectedSong;
export const getIsPlaying = state => selectSongsBranch(state).isPlaying;
export const getCurrentlyPlaying = state =>
  selectSongsBranch(state).currentlyPlaying;

export default {
  getIsLoading,
  getError,
  getSongs,
  getSelectedSong,
  getIsPlaying,
  getCurrentlyPlaying,
};
