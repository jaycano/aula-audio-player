import {
  GET_SONGS,
  GET_SONGS_SUCCESS,
  GET_SONGS_FAILURE,
  SELECT_SONG,
  PLAY_SONG,
  UPDATE_CURRENTLY_PLAYING,
} from "./songs.types";

export const updateCurrentlyPlaying = data => ({
  type: UPDATE_CURRENTLY_PLAYING,
  payload: data,
});

export const getSongs = () => ({
  type: GET_SONGS,
});

export const getSongsSuccess = songs => ({
  type: GET_SONGS_SUCCESS,
  payload: songs,
});

export const getSongsFailure = error => ({
  type: GET_SONGS_FAILURE,
  payload: error,
});

export const selectSong = song => ({
  type: SELECT_SONG,
  payload: { song },
});

export const playSong = () => ({
  type: PLAY_SONG,
});

export default {
  updateCurrentlyPlaying,
  getSongs,
  getSongsSuccess,
  getSongsFailure,
  selectSong,
  playSong,
};
