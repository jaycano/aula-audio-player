export const UPDATE_CURRENTLY_PLAYING = "[songs] UPDATE_CURRENTLY_PLAYING";
export const GET_SONGS = "[songs] GET_SONGS";
export const GET_SONGS_SUCCESS = "[songs] GET_SONGS_SUCCESS";
export const GET_SONGS_FAILURE = "[songs] GET_SONGS_FAILURE";
export const SELECT_SONG = "[songs] SELECT_SONG";
export const PLAY_SONG = "[songs] PLAY_SONG";
