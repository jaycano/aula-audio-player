import { authenticatedGet, getAuthStateFromStorage } from "../../auth/utils";
import io from "socket.io-client";

export default {
  getSongs: () => {
    return authenticatedGet("http://localhost:3000/api/song");
  },
  sendCurrentSong: song => {
    const socket = io("http://localhost:3000");
    const { username } = getAuthStateFromStorage();
    return socket.emit("play", {
      username,
      ...song,
      timestamp: Date.now(),
    });
  },
};
