import {
  UPDATE_CURRENTLY_PLAYING,
  GET_SONGS,
  GET_SONGS_SUCCESS,
  GET_SONGS_FAILURE,
  SELECT_SONG,
  PLAY_SONG,
} from "./songs.types";

const initialState = {
  isLoading: false,
  error: null,
  songs: [],
  selectedSong: null,
  isPlaying: false,
  currentlyPlaying: [],
};

const songsReducer = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_CURRENTLY_PLAYING:
      return {
        ...state,
        currentlyPlaying: [...state.currentlyPlaying, action.payload],
      };
    case GET_SONGS:
      return {
        ...state,
        isLoading: true,
      };
    case GET_SONGS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        error: null,
        songs: action.payload.songs,
      };
    case GET_SONGS_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: action.payload.error,
        songs: [],
      };
    case SELECT_SONG:
      return {
        ...state,
        selectedSong: action.payload.song,
        isPlaying: true,
      };
    case PLAY_SONG:
      return {
        ...state,
        isPlaying: !state.isPlaying,
      };
    default:
      return state;
  }
};

export default songsReducer;
