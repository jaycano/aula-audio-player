import { call, put, takeLatest } from "redux-saga/effects";
import Api from "./songs.api";
import actions from "./songs.actions";
import { GET_SONGS, SELECT_SONG } from "./songs.types";

function* getSongs(action) {
  try {
    const response = yield call(Api.getSongs);
    yield put(
      actions.getSongsSuccess({
        songs: response.data.docs,
      }),
    );
  } catch (e) {
    const { errors } = e.response.data;
    yield put(actions.getSongsFailure(errors));
  }
}

function* sendCurrentSong(action) {
  yield call(Api.sendCurrentSong, action.payload);
}

function* songsSagas() {
  yield takeLatest(GET_SONGS, getSongs);
  yield takeLatest(SELECT_SONG, sendCurrentSong);
}

export default songsSagas;
