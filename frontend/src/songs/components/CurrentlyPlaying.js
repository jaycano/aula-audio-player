import React, { Component } from "react";
import { List, ListItem, ListItemText, Typography } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";

const styles = theme => ({
  empty: {
    padding: theme.spacing.unit * 4,
    textAlign: "center",
  },
});

class CurrentlyPlaying extends Component {
  listPlay(play) {
    return (
      <ListItem
        button
        key={`${play.timestamp}${play.username}${play.song.title}`}
      >
        <ListItemText
          primary={`${play.song.title} - ${play.song.artist}`}
          secondary={play.username}
        />
      </ListItem>
    );
  }
  render() {
    const { currentlyPlaying, classes } = this.props;

    return (
      <List component="nav">
        {currentlyPlaying && currentlyPlaying.length > 0 ? (
          currentlyPlaying.map(this.listPlay)
        ) : (
          <Typography variant="subheading" className={classes.empty}>
            No song currently being played
          </Typography>
        )}
      </List>
    );
  }
}

export default withStyles(styles)(CurrentlyPlaying);
