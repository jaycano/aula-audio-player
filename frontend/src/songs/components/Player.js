import React, { Component } from "react";
import PropTypes from "prop-types";

class Player extends Component {
  constructor(props) {
    super(props);
    this.playerElement = React.createRef();
  }
  componentDidUpdate() {
    if (!this.props.songUrl) {
      return;
    }

    if (!this.props.isPlaying) {
      this.playerElement.pause();
    } else {
      this.playerElement.play();
    }
  }
  render() {
    const { songUrl } = this.props;
    return (
      <audio
        ref={element => (this.playerElement = element)}
        src={songUrl}
        autoPlay
      />
    );
  }
}

Player.propTypes = {
  isPlaying: PropTypes.bool.isRequired,
  songUrl: PropTypes.string,
};

export default Player;
