import React, { Component } from "react";
import PropTypes from "prop-types";

import {
  CircularProgress,
  IconButton,
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  Typography,
} from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import PlayCircleOutline from "@material-ui/icons/PlayCircleOutline";
import PauseCircleOutline from "@material-ui/icons/PauseCircleOutline";

const styles = theme => ({
  centeredCell: {
    textAlign: "center",
  },
  fullWidthCell: {
    textAlign: "center",
    padding: theme.spacing.unit * 5,
  },
  slowAnimation: {
    WebkitAnimationDuration: "2s",
    animationDuration: "2s",
  },
});

class Playlist extends Component {
  componentDidMount() {
    this.props.getSongs();
  }

  toggleSong(clickedSong, selectedSong) {
    if (!selectedSong || clickedSong.url !== selectedSong.url) {
      this.props.selectSong(clickedSong);
    } else {
      this.props.playSong();
    }
  }

  renderPlaylist(songs, selectedSong, isPlaying) {
    return songs.map(song => {
      return (
        <TableRow key={song.id}>
          <TableCell
            padding="dense"
            className={this.props.classes.centeredCell}
          >
            <IconButton
              color="primary"
              onClick={() => this.toggleSong(song, selectedSong)}
            >
              {selectedSong && selectedSong.url === song.url ? (
                <PauseCircleOutline
                  className={
                    isPlaying
                      ? ""
                      : `animated infinite flash ${
                          this.props.classes.slowAnimation
                        }`
                  }
                />
              ) : (
                <PlayCircleOutline />
              )}
            </IconButton>
          </TableCell>
          <TableCell component="th" scope="row">
            {song.title}
          </TableCell>
          <TableCell>{song.artist}</TableCell>
          <TableCell>{song.album}</TableCell>
          <TableCell>{song.year}</TableCell>
        </TableRow>
      );
    });
  }
  renderEmptyPlaylist() {
    return (
      <TableRow>
        <TableCell colSpan={5} className={this.props.classes.fullWidthCell}>
          <Typography variant="title">No songs available</Typography>
        </TableCell>
      </TableRow>
    );
  }
  renderLoading() {
    return (
      <TableRow>
        <TableCell colSpan={5} className={this.props.classes.fullWidthCell}>
          <CircularProgress size={50} />
        </TableCell>
      </TableRow>
    );
  }
  renderBody(songs, selectedSong, isLoading, isPlaying) {
    if (isLoading) {
      return this.renderLoading();
    } else if (!songs || songs.length < 1) {
      return this.renderEmptyPlaylist();
    } else {
      return this.renderPlaylist(songs, selectedSong, isPlaying);
    }
  }

  render() {
    const { songs, selectedSong, isLoading, isPlaying } = this.props;

    return (
      <Table>
        <TableHead>
          <TableRow>
            <TableCell padding="dense">&nbsp;</TableCell>
            <TableCell>Title</TableCell>
            <TableCell>Artist</TableCell>
            <TableCell>Album</TableCell>
            <TableCell>Year</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {this.renderBody(songs, selectedSong, isLoading, isPlaying)}
        </TableBody>
      </Table>
    );
  }
}

Playlist.propTypes = {
  getSongs: PropTypes.func.isRequired,
  selectSong: PropTypes.func.isRequired,
  playSong: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
  songs: PropTypes.arrayOf(PropTypes.object),
  selectedSong: PropTypes.object,
  isPlaying: PropTypes.bool.isRequired,
};

export default withStyles(styles)(Playlist);
