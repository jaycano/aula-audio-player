import React, { Component } from "react";
import { connect } from "react-redux";

import { Grid, Paper, Typography } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";

import Playlist from "../components/Playlist";
import Player from "../components/Player";
import CurrentlyPlaying from "../components/CurrentlyPlaying";

import { actions, selectors } from "../redux";

import io from "socket.io-client";

const styles = theme => ({
  layout: {
    width: "auto",
    padding: `${theme.spacing.unit * 8}px 0`,
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(1100 + theme.spacing.unit * 3 * 2)]: {
      width: 1100,
      marginLeft: "auto",
      marginRight: "auto",
    },
  },

  marginBottom: {
    marginBottom: theme.spacing.unit * 8,
  },
});

class Songs extends Component {
  componentDidMount() {
    const socket = io("http://localhost:3000");

    socket.on("play", data => {
      this.props.updateCurrentlyPlaying({ ...data });
    });
  }
  render() {
    const {
      classes,
      getSongs,
      selectSong,
      playSong,
      songs,
      isLoading,
      song,
      isPlaying,
      currentlyPlaying,
    } = this.props;
    return (
      <div className={classes.layout}>
        <Grid container spacing={24}>
          <Grid item xs={12}>
            <Player songUrl={song ? song.url : null} isPlaying={isPlaying} />
            <Typography variant="title">Playlist</Typography>
            <Paper className={classes.marginBottom}>
              <Playlist
                getSongs={getSongs}
                selectSong={selectSong}
                playSong={playSong}
                songs={songs}
                isLoading={isLoading}
                selectedSong={song}
                isPlaying={isPlaying}
              />
            </Paper>
            <Typography variant="title">Currently Playing</Typography>
            <Paper>
              <CurrentlyPlaying currentlyPlaying={currentlyPlaying} />
            </Paper>
          </Grid>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  songs: selectors.getSongs(state),
  song: selectors.getSelectedSong(state),
  isPlaying: selectors.getIsPlaying(state),
  isLoading: selectors.getIsLoading(state),
  currentlyPlaying: selectors.getCurrentlyPlaying(state),
});

const mapDispatchToProps = dispatch => ({
  getSongs: () => {
    dispatch(actions.getSongs());
  },
  selectSong: songUrl => {
    dispatch(actions.selectSong(songUrl));
  },
  playSong: () => {
    dispatch(actions.playSong());
  },
  updateCurrentlyPlaying: (username, song) => {
    dispatch(actions.updateCurrentlyPlaying(username, song));
  },
});

export default withStyles(styles)(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(Songs),
);
